//开票相关_开票
API_POST_INVOICE: {
url: 'invoice',
    config: {
        method: 'post',
        host: 'xs_promotion_center'
    }
},
//开票相关_获取发票订单详情
API_GET_INVOICE_DETAIL: {
url: 'invoice/detail',
    config: {
        method: 'get',
        host: 'xs_promotion_center'
    }
},
//开票相关_获取发票订单列表
API_GET_INVOICE_LIST: {
url: 'invoice/list',
    config: {
        method: 'get',
        host: 'xs_promotion_center'
    }
},
