;[
  {
    title: '直降促销API',
    apis: [
      {
        type: 'post',
        path: '/common/excel/shop-product-info',
        markdown: '批量导入店铺商品信息',
        params: '',
        note: '直降促销API_批量导入店铺商品信息'
      },
      {
        type: 'get',
        path: '/common/products',
        markdown: '获取商品信息',
        params: '',
        note: '直降促销API_获取商品信息'
      },
      {
        type: 'post',
        path: '/common/products',
        markdown: '获取商品信息',
        params: '',
        note: '直降促销API_获取商品信息'
      },
      {
        type: 'get',
        path: '/common/shops',
        markdown: '获取门店信息',
        params: '',
        note: '直降促销API_获取门店信息'
      },
      {
        type: 'post',
        path: '/common/syn-to-third-party',
        markdown: '同步到第三方',
        params: '',
        note: '直降促销API_同步到第三方'
      },
      {
        type: 'post',
        path: '/yhPluPromotion/aboActivity',
        markdown: '批量取消活动',
        params: 'activityCodes,cancelReason',
        note: '直降促销API_批量取消活动'
      },
      {
        type: 'post',
        path: '/yhPluPromotion/addShopInfo',
        markdown: '门店信息excel导入接口',
        params: '',
        note: '直降促销API_门店信息excel导入接口'
      },
      {
        type: 'post',
        path: '/yhPluPromotion/pluProAddRule',
        markdown: '直降促销活动规则添加接口',
        params:
          'activityBeginTime,activityDesc,activityEndTime,activityName,activityType,channelCodes,discountCommitment,discountCommitmentForm,memberType,productsVOList,businessPractice,discountAmount,discountPrice,discountRate,newPurchasingPrice,oldPurchasingPrice,oldSellingPrice,productBarCode,productBrand,productCategory,productCode,productName,supplierCode,promotionLevel,purchaseLimitation,shopsVOList,provinceCode,provinceName,shopCode,shopName,supplierDiscount',
        note: '直降促销API_直降促销活动规则添加接口'
      },
      {
        type: 'post',
        path: '/yhPluPromotion/queryPluPro',
        markdown: '直降促销活动查询',
        params:
          'activityBeginTime,activityEndTime,discountCommitment,discountCommitmentForm,memberType,pageNo,pageSize,promotionCode,promotionName,ruleType',
        note: '直降促销API_直降促销活动查询'
      },
      {
        type: 'post',
        path: '/yhPluPromotion/queryProductDetail',
        markdown: '商品详情查看',
        params:
          'activityBeginTime,activityEndTime,discountCommitment,discountCommitmentForm,memberType,pageNo,pageSize,promotionCode,promotionName,ruleType',
        note: '直降促销API_商品详情查看'
      },
      {
        type: 'post',
        path: '/yhPluPromotion/queryProductInfo',
        markdown: '获取商品信息',
        params:
          'businessPractice,discountAmount,discountPrice,discountRate,newPurchasingPrice,oldPurchasingPrice,oldSellingPrice,productBarCode,productBrand,productCategory,productCode,productName,supplierCode',
        note: '直降促销API_获取商品信息'
      },
      {
        type: 'post',
        path: '/yhPluPromotion/queryShopDetail',
        markdown: '门店详情查看',
        params:
          'activityBeginTime,activityEndTime,discountCommitment,discountCommitmentForm,memberType,pageNo,pageSize,promotionCode,promotionName,ruleType',
        note: '直降促销API_门店详情查看'
      },
      {
        type: 'post',
        path: '/yhPluPromotion/queryShopInfo',
        markdown: '获取门店信息',
        params: 'provinceCode,provinceName,shopCode,shopName',
        note: '直降促销API_获取门店信息'
      }
    ]
  }
]
