module.exports = {
  //初始url
  // url: 'http://10.0.91.116/b2bmall/swagger-ui.html#/',
  //   url: 'http://10.19.12.183:8080/swagger-ui.html#/',
  //   url: 'http://10.0.91.144/b2bmall/swagger-ui.html#/',
  // url: 'http://127.0.0.1:8080/',
  // url: 'http://10.0.91.144/b2bmall/swagger-ui.html#/',
  // url:
  //   "http://xszt-sit.yh-soi-view-promotion.xszt-001.sitgw.yonghui.cn/swagger-ui.html#/",
  url:
    "http://xszt-sit.yh-soi-view-saleorder.xszt-001.sitgw.yonghui.cn/swagger-ui.html#/",
  // url:
  //   'http://xszt-dev.yh-soi-manage-productcenter.devgw.yonghui.cn/swagger-ui.html#/',
  host: "xs_promotion_center",
  exportDir: "export",
  defaultPath: "./export/defaultApi.js",
  ownPath: "./export/ownApi.js",
  dataPath: "./export/data.js",
  templete:
    "`API_${type.toUpperCase()}_${path.match(/[^/{}]+(?=($|(.{.+})+$))/)[0].toUpperCase()}: {path: '${path.substring(1)}',config: {method: '${type}',host: 'normal'}},\n`",
  test: `API_#{type.toUpperCase()}_#{path.toUpperCase()}: {
    path: '#{path.substring(1)}',
        config: {
            method: '#{type}',
            host: 'normal'
        }
    },\n`
};
